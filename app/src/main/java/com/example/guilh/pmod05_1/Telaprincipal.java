package com.example.guilh.pmod05_1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Telaprincipal extends AppCompatActivity implements View.OnClickListener {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_telaprincipal);

        Button botao = (Button)findViewById(R.id.btProximo);
        botao.setOnClickListener(this);
    }

    public void onClick(View v) {
        Intent intencao = new Intent(this, Tela2.class);

        Bundle parametros = new Bundle();
        parametros.putString("parametro1", "Sou o parametro 1");
        parametros.putString("parametro2", "Sou o parametro 2");
        intencao.putExtras(parametros);

        startActivity(intencao);
    }
}
