package com.example.guilh.pmod05_1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Tela2 extends AppCompatActivity implements View.OnClickListener {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela2);

        Intent intencao = getIntent();
        Bundle parametros = intencao.getExtras();
        String parametro1 = parametros.getString("parametro1");
        String parametro2 = parametros.getString("parametro2");

        TextView status = (TextView)findViewById(R.id.tvStatus);
        status.setText("Chegou: 1: '" + parametro1 + "' e 2: '" + parametro2 + "'");

        Button botao = (Button)findViewById(R.id.btAnterior);
        botao.setOnClickListener(this);
    }

    public void onClick(View v) {
        finish();
    }
}
